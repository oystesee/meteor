// new collection 
 export const Posts = new Mongo.Collection('Posts');

/*
    Collection må ha en schema som kan validere input
    denne her funker ikke desverre....
    Posts.schema = new SimpleSchema({
        content : {type : String},
    });
*/

//Permissions for the collection
Posts.allow({
    insert: function(){
        return true; 
    }, 
    update : function(){
        return true; 
    },
    remove : function(){
        return true; 
    }
});