/*
only a simple search-bar at the moment
*/
Search = React.createClass({
    render() {
        function startingSearch(){
            Materialize.toast('Behandler søket ditt');
        }
        // google maps setup, currentley not working
        if (Meteor.isClient) {
            Meteor.startup(function () {
                GoogleMaps.load();
            });
        }
        var searchStyle = {
            backgroundColor: "#3f51b5",
            paddingLeft: "12px"
        };
        return (
              <div>  
                    <div className="nav-wrapper" style={searchStyle}>
                        <form>
                            <div className="input-field">
                                <input id="search" type="search" required />
                                <label for="search"><i className="material-icons">search</i></label>
                                <i className="material-icons">close</i>
                            </div>
                        </form>
                    </div>
                    <button type='button' className="waves-effect waves-light btn btn-block" onClick="startingSearch()"> Search</button>
            </div>
        );
    }
});

