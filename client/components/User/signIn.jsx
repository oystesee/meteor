SignIn = React.createClass({
    onSubmit(e){
        e.preventDefault();
        // gets input 
        var el = $(e.target);
        var email = el.find("#email").val();
        var password = el.find("#password").val();

        // logs in the user 
        Meteor.loginWithPassword(email, password, (er) => {
            //prints error if issues with credentials
            if (er) {
                console.log(er);
                Materialize.toast(er.reason, 4000);
            } else {
                //Redirect 
                alert("you are logged in"); 
                FlowRouter.go('/loggedin');
            }
        });
    },
    render(){
        return(
            <div className="row">
				<h4 className="text-center">Log inn</h4>
				<form onSubmit={this.onSubmit} className="col offset-s4 s4">
					<div className="row">
						<div className="input-field col s12">
							<input id="email" type="text" className="validate" />
							<label htmlFor="email">E-post</label>
						</div>
					</div>
					<div className="row">
						<div className="input-field col s12">
							<input id="password" type="password" className="validate" />
							<label htmlFor="password">Passord</label>
						</div>
                        <div className="row">
						<button className="waves-effect waves-light btn btn-block">Log inn</button>
					</div>
					</div>
				</form>
			</div>
        )
    }
})