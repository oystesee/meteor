InsertData = React.createClass({
        insertToCollection(event){
            console.log("entering insert function");
            Posts1 = new Mongo.Collection('Posts1');
            event.preventDefault();
            var content = $('#post1').val();
            console.log(content);
            
            Posts1.insert({
                content: content, 
                dateAdded: new Date(),
            });
        },
    render(){
        return(
            <div>
                <form onSubmit={this.insertToCollection}>
                    <textarea placeholder="type a post" className="input-field" id="post1"> </textarea>
                    <button className="waves-effect waves-light btn btn-block"> Submit </button>
                </form>
            <importer />

                <DisplayData />
            </div>
        );
    }
});