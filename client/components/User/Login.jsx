/*
This page shoulb display the users personal data from places they have eaten and want to store for later
*/

Login = React.createClass({
    render(){
        return(
            <div>
                <h1> Velkommen til din side!</h1>
                <p> Her kan du legge til og fjerne alle mulige spisesteder i hele Trondheim by </p>
                // this is only a possible displayment of how the users data is presented
                <table className="highlight">
                    <thead>
                        <tr>
                            <th data-field="id">Navn</th>
                            <th data-field="name">Type mat</th>
                            <th data-field="price">Vurdering</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Alvin</td>
                            <td>Eclair</td>
                            <td>$0.87</td>
                        </tr>
                        <tr>
                            <td>Alan</td>
                            <td>Jellybean</td>
                            <td>$3.76</td>
                        </tr>
                        <tr>
                            <td>Jonathan</td>
                            <td>Lollipop</td>
                            <td>$7.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});