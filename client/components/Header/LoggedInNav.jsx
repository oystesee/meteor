
LoggedInNav = React.createClass({
	render() {
		return (
			  <div>
          <ul className="right hide-on-med-and-down">
            <li><a href="/loggedIn">My Places</a></li>
            <li><a href="/search">Search</a></li>
            <li><a href="/" onClick={this.props.logout}>Log out</a></li>
            <li><a href="/collectionTest"> Post data</a></li>
          </ul>
          <ul className="side-nav" id="mobile-demo">
            <li><a href="/">Home</a></li>
            <li><a href="/search">Search</a></li>
            <li><a href="/" onClick={this.logout}>Log out</a></li>
          </ul>
        </div>
		);
	}
});